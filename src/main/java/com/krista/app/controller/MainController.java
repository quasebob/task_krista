package com.krista.app.controller;

import com.krista.app.entities.Coordinate;
import com.krista.app.repositories.CoordinateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class MainController {

    @Autowired
    private CoordinateRepository coordinateRepository;

    @GetMapping("/")
    public String mainPage(){ return "buttons"; }

    @GetMapping("/database")
    public String dbPage(Model model){

        List<Coordinate> allCoordinates = coordinateRepository.findAll();
        model.addAttribute("coordinates",allCoordinates);
        return "database";
    }
    @PostMapping
    public String add(@RequestParam int x, @RequestParam int y, Model model){

        Coordinate coordinate = new Coordinate(x,y);
        coordinateRepository.save(coordinate);
        List<Coordinate> allCoordinates = coordinateRepository.findAll();
        model.addAttribute("coordinates",allCoordinates);
        return "buttons";
    }

}
