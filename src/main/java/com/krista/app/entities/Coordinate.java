package com.krista.app.entities;


import jakarta.persistence.*;

@Entity
@Table(name = "coordinates")
public class Coordinate {
    @Id

    @Column(name = "x")
    private int x;
    @Column(name = "y")
    private int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Coordinate(){}

    public Coordinate( int x, int y){
        this.x = x;
        this.y = y;
    }


}
