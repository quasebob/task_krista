package com.krista.app.repositories;

import com.krista.app.entities.Coordinate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface CoordinateRepository extends JpaRepository<Coordinate,Long> {

}
